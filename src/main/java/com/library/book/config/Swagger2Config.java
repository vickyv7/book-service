package com.library.book.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
@Configuration
@EnableSwagger2
public class Swagger2Config {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
            .apis(RequestHandlerSelectors.basePackage("com.library.book"))
            .paths(PathSelectors.regex("/.*"))
            .build().apiInfo(apiEndPointsInfo()).pathMapping("/");
    }
    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("Book Service REST API")
            .description("Library book module's REST API")
            .contact(new Contact("Vikrama Vembuluru", "www.example.net", "vikramkumarv7@gmail.com"))
            .version("1.0.0")
            .build();
    }
}