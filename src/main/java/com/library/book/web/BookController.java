package com.library.book.web;

import com.library.book.exception.BookException;
import com.library.book.model.Book;
import com.library.book.model.Response;
import com.library.book.service.BookService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("Book service Rest controller")
@RequestMapping("/v1/books/")
@RestController
public class BookController {
	
	private static final Logger logger = LoggerFactory.getLogger(BookController.class);

	@Autowired
	private BookService bookService;
	
	@GetMapping
	public ResponseEntity<List<Book>> getAllBook(){
    	logger.info("Returning all the Book's");
		return new ResponseEntity<>(bookService.getAllBook(), HttpStatus.OK);
	}
	
    @GetMapping(value = "{id}")
	public ResponseEntity<Book> getBookById(@PathVariable("id") long id) {
    	logger.info("Book id to return " + id);
		return new ResponseEntity<>(bookService.getBookById(id), HttpStatus.OK);
	}

    @DeleteMapping(value = "{id}")
	public ResponseEntity<Response> removeBookById(@PathVariable("id") long id) throws BookException {
    	logger.info("Book id to remove " + id);
    	Book book = bookService.getBookById(id);
    	if (book == null || book.getId() <= 0){
            throw new BookException("Book to delete doesn't exist");
    	}
		bookService.removeBook(book);
		return new ResponseEntity<>(new Response(HttpStatus.OK.value(), "Book has been deleted"), HttpStatus.OK);
	}
    
    @PostMapping
   	public ResponseEntity<Book> saveBook(@RequestBody Book book) throws BookException{
    	logger.info("Payload to save " + book);
    	if (book.getId()>0){
            throw new BookException("Payload malformed, id must not be defined");
    	}
		return new ResponseEntity<>(bookService.saveBook(book), HttpStatus.OK);
   	}
    
    @PutMapping
   	public ResponseEntity<Book>  updateBook(@RequestBody Book book) throws BookException {
    	logger.info("Payload to update " + book);
    	Book b = bookService.getBookById(book.getId());
    	if (b.getId() <= 0){
            throw new BookException("Book to update doesn't exist");
    	}
		return new ResponseEntity<>(bookService.saveBook(book), HttpStatus.OK);
   	}
	
}