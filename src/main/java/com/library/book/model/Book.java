package com.library.book.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@NoArgsConstructor
public class Book extends BaseEntity{

    @NonNull
    private String name;

    @NonNull
    @Lob
    private String description;

    @NonNull
    private String publisher;

    @NonNull
    private int year;

    private boolean isAvailable=true;
}
