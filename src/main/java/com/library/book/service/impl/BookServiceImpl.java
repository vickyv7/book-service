package com.library.book.service.impl;

import com.library.book.exception.ResourceNotFoundException;
import com.library.book.model.Book;
import com.library.book.repo.BookRepository;
import com.library.book.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookRepository bookRepository;
	
	@Override
	public List<Book> getAllBook() {
		return bookRepository.findAll();
	}

	@Override
	public Book getBookById(long id) {
		return bookRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Book with given id not found"));
	}

	@Override
	public Book saveBook(Book book) {
		return bookRepository.save(book);
	}

	@Override
	public void removeBook(Book book) {
		bookRepository.delete(book);
	}
	

}