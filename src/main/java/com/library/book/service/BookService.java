package com.library.book.service;

import com.library.book.model.Book;

import java.util.List;

public interface BookService {
	 List<Book> getAllBook();
	 Book getBookById(long id);
	 Book saveBook(Book book);
	 void removeBook(Book book);
}