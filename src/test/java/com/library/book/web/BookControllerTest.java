package com.library.book.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.library.book.BookServiceApplication;
import com.library.book.model.Book;
import com.library.book.repo.BookRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BookServiceApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BookControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private BookRepository bookRepository;

    private List<Book> bookList;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        bookList = new ArrayList<>();
        bookList.add(new Book("Sample1", "Book Sample 1", "Publisher1", 2010));
        bookList.add(new Book("Sample2", "Book Sample 2", "Publisher2", 2011));
        bookList.add(new Book("Sample3", "Book Sample 3", "Publisher1", 2012));
        bookRepository.saveAll(bookList);
    }

    @After
    public void clearRecords() {
        bookRepository.deleteAll();
    }

    @Test
    public void verifyAllBookList() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/books/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3))).andDo(print());
    }

    @Test
    public void verifyBookById() throws Exception {
        long id = bookList.get(2).getId();
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/books/" + id).accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.available").exists())
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.description").value("Book Sample 3"))
                .andExpect(jsonPath("$.available").value(true))
                .andDo(print());
    }

    @Test
    public void verifyInvalidBookArgument() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/books/f").accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errorCode").value(400))
                .andExpect(jsonPath("$.message").value("The request could not be understood by the server due to malformed syntax."))
                .andDo(print());
    }

    @Test
    public void verifyInvalidBookId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/books/0").accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errorCode").value(404))
                .andExpect(jsonPath("$.message").value("Book with given id not found"))
                .andDo(print());
    }

    @Test
    public void verifyNullBook() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/books/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errorCode").value(404))
                .andExpect(jsonPath("$.message").value("Book with given id not found"))
                .andDo(print());
    }

    @Test
    public void verifyDeleteBook() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/v1/books/" + bookList.get(0).getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status").value(200))
                .andExpect(jsonPath("$.message").value("Book has been deleted"))
                .andDo(print());
    }

    @Test
    public void verifyInvalidBookIdToDelete() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/v1/books/9").accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errorCode").value(404))
                .andExpect(jsonPath("$.message").value("Book with given id not found"))
                .andDo(print());
    }


    @Test
    public void verifySaveBook() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/books/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(convertBookToJson(new Book("TestSave", "New Book Sample", "TestPublisher", 2019))))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.available").exists())
                .andExpect(jsonPath("$.description").value("New Book Sample"))
                .andExpect(jsonPath("$.available").value(true))
                .andDo(print());
    }

    @Test
    public void verifyMalformedSaveBook() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/books/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(convertBookToJson(bookList.get(0))))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.errorCode").value(404))
                .andExpect(jsonPath("$.message").value("Payload malformed, id must not be defined"))
                .andDo(print());
    }

    @Test
    public void verifyUpdateBook() throws Exception {
        Book book = bookList.get(0);
        book.setDescription("Update Book Description");
        book.setAvailable(false);
        mockMvc.perform(MockMvcRequestBuilders.put("/v1/books/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(convertBookToJson(book)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.available").exists())
                .andExpect(jsonPath("$.id").value(book.getId()))
                .andExpect(jsonPath("$.description").value("Update Book Description"))
                .andExpect(jsonPath("$.available").value(false))
                .andDo(print());
    }

    @Test
    public void verifyInvalidBookUpdate() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/v1/books/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"idd\": \"8\", \"description\" : \"New Book Sample\", \"available\" : \"false\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    private String convertBookToJson(Book book) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(book);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

}