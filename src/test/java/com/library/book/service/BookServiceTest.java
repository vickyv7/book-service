package com.library.book.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.library.book.model.Book;
import com.library.book.repo.BookRepository;
import com.library.book.service.impl.BookServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
public class BookServiceTest {
	
	@Mock
	private BookRepository bookRepository;
	
	@InjectMocks
	private BookServiceImpl bookService;
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetAllBook(){
		List<Book> bookList = new ArrayList<>();
		bookList.add(new Book("Sample1","Book Sample 1","Publisher1",2010));
		bookList.add(new Book("Sample2","Book Sample 2","Publisher2",2011));
		bookList.add(new Book("Sample3","Book Sample 3","Publisher1",2012));
		when(bookRepository.findAll()).thenReturn(bookList);
		
		List<Book> result = bookService.getAllBook();
		assertEquals(3, result.size());
	}
	
	@Test
	public void testGetBookById(){
		Book book = new Book("Sample1","Book Sample 1","Publisher1",2010);
		book.setId(1L);
		when(bookRepository.findById(1L)).thenReturn(Optional.of(book));
		Book result = bookService.getBookById(1);
		assertEquals(1L, result.getId());
		assertEquals("Book Sample 1", result.getDescription());
		assertTrue(result.isAvailable());
	}
	
	@Test
	public void saveBook(){
		Book book = new Book("Sample8","Book Sample 8","Publisher1",2010);
		book.setId(8L);
		book.setAvailable(false);
		when(bookRepository.save(book)).thenReturn(book);
		Book result = bookService.saveBook(book);
		assertEquals(8, result.getId());
		assertEquals("Sample8",result.getName());
		assertEquals("Book Sample 8", result.getDescription());
		assertFalse(result.isAvailable());
	}
	
	@Test
	public void removeBook(){
		Book book = new Book("Sample8","Book Sample 8","Publisher1",2010);
		book.setId(8L);
		bookService.removeBook(book);
        verify(bookRepository, times(1)).delete(book);
	}
	
	

}